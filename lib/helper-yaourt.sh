#!/bin/bash
#yaourt

# run command <OPTIONS> <PACKAGES(s)>
helper() { yaourt $1 $2 ;}

helper_islocal() { yaourt -Qs "^${1}$" | head -n 1 | grep "^local" &>/dev/null ;}

helper_sync() { yaourt $1 $2 ;}

helper_pkgisinstalled() { yaourt -Qq "$1" &>/dev/null ;}

helper_listdesc() { package-query -Sl -f '%n - %d' ;}

# return config file
# if param, return model file in /etc/
helper_getconffile() { [ -n "$1" ] && echo "/etc/yaourtrc" || echo "$HOME/.config/yaourt/yaourtrc" ;}

helperrc_edit() {
    [[ -z "$EDITOR" ]] && EDITOR='nano'
    local file=$(helper_getconffile)
    [ ! -f "$file" ] && cp -v "$(helper_getconffile 'model')" "$file"
    $EDITOR "$file"
}
