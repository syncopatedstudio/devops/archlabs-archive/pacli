#!/bin/bash
#pacaur

# run command <OPTIONS> <PACKAGES(s)>
helper() {
    local options="$1" packages="$2"
    options="${options/'--nocolor'/}"
    pacaur $options $packages
}

helper_islocal() { expac -Q "%r" "^${1}$" | head -n 1 | grep "^local" &>/dev/null ;}

helper_sync() {
    local options="$1" packages="$2"
    options="${options/'--nocolor'/}"
    pacaur $options $packages
}

helper_pkgisinstalled() { pacaur -Qq "$1" &>/dev/null ;}

# list all packages with description
helper_listdesc() { expac -S "%n : %d" ;}

# return config file
# if param, return model file in /etc/
helper_getconffile() { [ -n "$1" ] && echo "/etc/xdg/pacaur/config" || echo "$HOME/.config/pacaur/config" ;}

helperrc_edit() {
    [[ -z "$EDITOR" ]] && EDITOR='nano'
    local file=$(helper_getconffile)
    [ ! -f "$file" ] && cp -v "$(helper_getconffile 'model')" "$file"
    $EDITOR "$file"
}
